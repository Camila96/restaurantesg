from django.db import models
from django.utils import timezone

# Create your models here.

class Provincia(models.Model):
    id_sg = models.AutoField(primary_key=True)
    nombre_sg = models.CharField(max_length=150)
    region_sg = models.CharField(max_length=150)
    clima_sg = models.CharField(max_length=100)
    gastronomialocal_sg = models.CharField(max_length=150)

    def __str__(self):
       fila="{0}: {1} {2} {3}"
       return fila.format(self.id_sg,self.nombre_sg,self.region_sg,self.clima_sg)

class Cliente(models.Model):
    id_sg = models.AutoField(primary_key=True)
    nombre_sg = models.CharField(max_length=150)
    direccion_sg = models.CharField(max_length=150)
    telefono_sg = models.CharField(max_length=100)
    provincia = models.ForeignKey(Provincia, null=True, blank=True, on_delete=models.PROTECT)

    def __str__(self):
        fila = "{0}: {1} {2} {3}"
        return fila.format(self.id_sg, self.nombre_sg, self.direccion_sg, self.telefono_sg)



class Pedido(models.Model):
    id_sg = models.AutoField(primary_key=True)
    cliente_sg = models.CharField(max_length=150)
    fechapedido_sg = models.DateTimeField(auto_now_add=True)
    estado_sg = models.CharField(max_length=100)
    cliente = models.ForeignKey(Cliente, null=True, blank=True, on_delete=models.PROTECT)

    def __str__(self):
        fila = "{0}: {1} {2} {3}"
        return fila.format(self.id_sg, self.cliente_sg, self.fechapedido_sg, self.estado_sg)


class Tipo(models.Model):
    id_sg = models.AutoField(primary_key=True)
    nombre_sg = models.CharField(max_length=50, unique=True)
    descripcion_sg = models.TextField(blank=True, null=True)
    contenidonutricional_sg = models.TextField(blank=True, null=True)

    def __str__(self):
        fila = "{0}: {1} {2}"
        return fila.format(self.id_sg, self.nombre_sg, self.descripcion_sg)



class Platillo(models.Model):
    id_sg = models.AutoField(primary_key=True)
    nombreplatillo_sg = models.CharField(max_length=100)
    descripcionplatillo_sg = models.TextField(blank=True, null=True)
    precio_sg = models.DecimalField(max_digits=10, decimal_places=2)
    tipo = models.ForeignKey(Tipo, null=True, blank=True, on_delete=models.PROTECT)
    fotografia=models.FileField(upload_to='platillo',null=True,blank=True)

    def __str__(self):
        fila = "{0}: {1} {2} {3}"
        return fila.format(self.id_sg, self.nombreplatillo_sg, self.descripcionplatillo_sg, self.precio_sg)



class Detalle(models.Model):
    id_sg = models.AutoField(primary_key=True)
    observaciones_sg = models.TextField(blank=True, null=True)
    fecha_sg = models.DateField()
    total_sg = models.CharField(max_length=8, default='0')
    pedido=models.ForeignKey(Pedido, null=True, blank=True, on_delete=models.PROTECT)
    platillo=models.ForeignKey(Platillo, null=True, blank=True, on_delete=models.PROTECT)



class Ingrediente(models.Model):
    id_sg = models.AutoField(primary_key=True)
    nombreingrediente_sg = models.CharField(max_length=100, unique=True)
    descripcioningrediente_sg = models.TextField(blank=True, null=True)
    cantidaddisponible_sg = models.DecimalField(max_digits=10, decimal_places=2)
    proveedor_sg = models.CharField(max_length=255, default='Proveedor por defecto')

class Receta(models.Model):
    id_sg = models.AutoField(primary_key=True)
    cantidad_sg = models.DecimalField(max_digits=10, decimal_places=2)
    nombrereceta_sg = models.CharField(max_length=255,default='Nombre por defecto')
    descripcion_sg = models.TextField(default='Descripción por defecto')
    tiempopreparacion_sg = models.TimeField()
    ingrediente=models.ForeignKey(Ingrediente, null=True, blank=True, on_delete=models.PROTECT)
    platillo=models.ForeignKey(Platillo, null=True, blank=True, on_delete=models.PROTECT)
