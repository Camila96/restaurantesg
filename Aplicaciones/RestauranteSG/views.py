from django.shortcuts import render , redirect
from . import views
from .models import *

# Importacion para los mensajes de confirmacion.
from django.contrib import messages
#IMPORTAR LIBRERIAS PARA HACER EL CRUD DE LA FOTO
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
# Create your views here.

def listadoProvincia(request):
    provinciasBdd=Provincia.objects.all()
    return render(request, 'listadoProvincia.html', {'provincias':provinciasBdd,})

def guardarProvincia(request):
    nombre_sg = request.POST["nombre_sg"]
    region_sg = request.POST["region_sg"]
    clima_sg = request.POST["clima_sg"]
    gastronomialocal_sg= request.POST["gastronomialocal_sg"]

    nuevoProvincia = Provincia.objects.create(

        nombre_sg=nombre_sg,
        region_sg=region_sg,
        clima_sg=clima_sg,
        gastronomialocal_sg=gastronomialocal_sg,
    )
    messages.success(request, 'La Provincia ha sido guardado exitosamente')
    return redirect('/')

def eliminarProvincia(request,id_sg):
    provinciaEliminar=Provincia.objects.get(id_sg=id_sg)
    provinciaEliminar.delete()
    messages.success(request, 'La provincia ha sido eliminado exitosamente')
    return redirect('/')

def editarProvincia(request,id_sg):
    provinciaEditar=Provincia.objects.get(id_sg=id_sg)
    return render(request, 'editarProvincia.html',{'provincia':provinciaEditar})


def procesarActualizacionProvincia(request):
    id_sg=request.POST["id_sg"]
    nombre_sg=request.POST["nombre_sg"]
    region_sg=request.POST["region_sg"]
    clima_sg=request.POST["clima_sg"]
    gastronomialocal_sg=request.POST["gastronomialocal_sg"]

    provinciaEditar=Provincia.objects.get(id_sg=id_sg)
    provinciaEditar.nombre_sg=nombre_sg
    provinciaEditar.region_sg=region_sg
    provinciaEditar.clima_sg=clima_sg
    provinciaEditar.gastronomialocal_sg=gastronomialocal_sg

    provinciaEditar.save()
    messages.success(request,
      'Provincia actualizada Exitosamente')
    return redirect('/')



def listadoCliente(request):
    clientesBdd=Cliente.objects.all()
    provinciasBdd=Provincia.objects.all()
    return render(request, 'listadoCliente.html', {'clientes':clientesBdd,'provincias':provinciasBdd})

def guardarCliente(request):
    id_sg_provincia=request.POST["id_sg_provincia"]
    provinciaSeleccionado=Provincia.objects.get(id_sg=id_sg_provincia)
    nombre_sg = request.POST["nombre_sg"]
    direccion_sg = request.POST["direccion_sg"]
    telefono_sg = request.POST["telefono_sg"]

    nuevoCliente = Cliente.objects.create(
    nombre_sg=nombre_sg,
    direccion_sg=direccion_sg,
    telefono_sg=telefono_sg,
    provincia=provinciaSeleccionado
    )
    messages.success(request, 'El cliente ha sido guardado exitosamente')
    return redirect('listado_cliente')

def eliminarCliente(request,id_sg):
    clienteEliminar=Cliente.objects.get(id_sg=id_sg)
    clienteEliminar.delete()
    messages.success(request, 'El cliente ha sido eliminado exitosamente')
    return redirect('listado_cliente')

def editarCliente(request,id_sg):
    clienteEditar=Cliente.objects.get(id_sg=id_sg)
    provinciasBdd=Provincia.objects.all()
    return render(request, 'editarCliente.html',{'cliente':clienteEditar, 'provincias':provinciasBdd})


def procesarActualizacionCliente(request):
    id_sg=request.POST["id_sg"]
    id_sg_provincia=request.POST["id_sg_provincia"]
    provinciaSeleccionado=Provincia.objects.get(id_sg=id_sg_provincia)
    nombre_sg=request.POST["nombre_sg"]
    direccion_sg=request.POST["direccion_sg"]
    telefono_sg=request.POST["telefono_sg"]

    clienteEditar=Cliente.objects.get(id_sg=id_sg)
    clienteEditar.provincia=provinciaSeleccionado
    clienteEditar.nombre_sg=nombre_sg
    clienteEditar.direccion_sg=direccion_sg
    clienteEditar.telefono_sg=telefono_sg

    clienteEditar.save()
    messages.success(request,
      'Cliente actualizado Exitosamente')
    return redirect('listado_cliente')


def listadoPedido(request):
    pedidosBdd=Pedido.objects.all()
    clientesBdd=Cliente.objects.all()
    return render(request, 'listadoPedido.html', {'pedidos':pedidosBdd,'clientes':clientesBdd})

def guardarPedido(request):
    id_sg_cliente = request.POST["id_sg_cliente"]
    clienteSeleccionado = Cliente.objects.get(id_sg=id_sg_cliente)


    cliente_sg = request.POST["cliente_sg"]
    fechapedido_sg = request.POST["fechapedido_sg"]
    estado_sg = request.POST["estado_sg"]

    nuevoPedido = Pedido.objects.create(
        cliente_sg=cliente_sg,
        fechapedido_sg=fechapedido_sg,
        estado_sg=estado_sg,
        cliente=clienteSeleccionado
    )
    messages.success(request, 'El pedido ha sido guardado exitosamente')
    return redirect('listado_pedido')


def eliminarPedido(request,id_sg):
    pedidoEliminar=Pedido.objects.get(id_sg=id_sg)
    pedidoEliminar.delete()
    messages.success(request, 'El pedido ha sido eliminado exitosamente')
    return redirect('listado_pedido')

def editarPedido(request, id_sg):
    pedidoEditar = Pedido.objects.get(id_sg=id_sg)
    clientesBdd = Cliente.objects.all()
    return render(request, 'editarPedido.html', {'pedido': pedidoEditar, 'clientes': clientesBdd})


def procesarActualizacionPedido(request):
    id_sg = request.POST["id_sg"]
    id_sg_cliente = request.POST["id_sg_cliente"]
    clienteSeleccionado = Cliente.objects.get(id_sg=id_sg_cliente)
    cliente_sg = request.POST["cliente_sg"]
    fechapedido_sg = request.POST["fechapedido_sg"]
    estado_sg = request.POST["estado_sg"]

    pedidoEditar = Pedido.objects.get(id_sg=id_sg)
    pedidoEditar.cliente=clienteSeleccionado
    pedidoEditar.cliente_sg = cliente_sg
    pedidoEditar.fechapedido_sg = fechapedido_sg
    pedidoEditar.estado_sg = estado_sg

    pedidoEditar.save()  # Guardar cambios en el objeto Pedido, no en el Cliente
    messages.success(request, 'Pedido actualizado Exitosamente')
    return redirect('listado_pedido')


def listadoTipo(request):
        tiposBdd=Tipo.objects.all()
        return render(request, 'listadoTipo.html', {'tipos':tiposBdd,})

def guardarTipo(request):
        nombre_sg= request.POST["nombre_sg"]
        descripcion_sg= request.POST["descripcion_sg"]
        contenidonutricional_sg= request.POST["contenidonutricional_sg"]

        nuevoTipo = Tipo.objects.create(
        nombre_sg=nombre_sg,
        descripcion_sg=descripcion_sg,
        contenidonutricional_sg=contenidonutricional_sg
        )
        messages.success(request, 'El tipo ha sido guardado exitosamente')
        return redirect('listado_tipo')

def eliminarTipo(request,id_sg):
        tipoEliminar=Tipo.objects.get(id_sg=id_sg)
        tipoEliminar.delete()
        messages.success(request, 'El tipo ha sido eliminado exitosamente')
        return redirect('listado_tipo')

def editarTipo(request,id_sg):
        tipoEditar=Tipo.objects.get(id_sg=id_sg)
        return render(request, 'editarTipo.html',{'tipo':tipoEditar})


def procesarActualizacionTipo(request):
        id_sg=request.POST["id_sg"]
        nombre_sg=request.POST["nombre_sg"]
        descripcion_sg=request.POST["descripcion_sg"]
        contenidonutricional_sg=request.POST["contenidonutricional_sg"]

        tipoEditar=Tipo.objects.get(id_sg=id_sg)
        tipoEditar.nombre_sg=nombre_sg
        tipoEditar.descripcion_sg=descripcion_sg
        tipoEditar.contenidonutricional_sg=contenidonutricional_sg

        tipoEditar.save()
        messages.success(request,
          'Tipo actualizado Exitosamente')
        return redirect('listado_tipo')

def listadoPlatillo(request):
        platillosBdd=Platillo.objects.all()
        tiposBdd=Tipo.objects.all()
        return render(request, 'listadoPlatillo.html', {'platillos':platillosBdd,'tipos':tiposBdd})

def guardarPlatillo(request):
        id_sg_tipo=request.POST["id_sg_tipo"]
        tipoSeleccionado=Tipo.objects.get(id_sg=id_sg_tipo)
        nombreplatillo_sg= request.POST["nombreplatillo_sg"]
        descripcionplatillo_sg= request.POST["descripcionplatillo_sg"]
        precio_sg= request.POST["precio_sg"]
        fotografia=request.FILES.get("fotografia")

        nuevoPlatillo = Platillo.objects.create(
        nombreplatillo_sg=nombreplatillo_sg,
        descripcionplatillo_sg=descripcionplatillo_sg,
        precio_sg=precio_sg,
        tipo=tipoSeleccionado,
        fotografia=fotografia
        )
        messages.success(request, 'El platillo ha sido guardado exitosamente')
        return redirect('listado_platillo')

def eliminarPlatillo(request,id_sg):
        platilloEliminar=Platillo.objects.get(id_sg=id_sg)
        platilloEliminar.delete()
        messages.success(request, 'El platillo ha sido eliminado exitosamente')
        return redirect('listado_platillo')

def editarPlatillo(request,id_sg):
        platilloEditar=Platillo.objects.get(id_sg=id_sg)
        tiposBdd=Tipo.objects.all()
        return render(request, 'editarPlatillo.html',{'platillo':platilloEditar,'tipos':tiposBdd})


def procesarActualizacionPlatillo(request):
        id_sg=request.POST["id_sg"]
        id_sg_tipo=request.POST["id_sg_tipo"]
        tipoSeleccionado=Tipo.objects.get(id_sg=id_sg_tipo)
        nombreplatillo_sg=request.POST["nombreplatillo_sg"]
        descripcionplatillo_sg=request.POST["descripcionplatillo_sg"]
        precio_sg=request.POST["precio_sg"]
        fotografia=request.FILES.get("fotografia")

        platilloEditar=Platillo.objects.get(id_sg=id_sg)
        platilloEditar.tipo=tipoSeleccionado
        platilloEditar.nombreplatillo_sg=nombreplatillo_sg
        platilloEditar.descripcionplatillo_sg=descripcionplatillo_sg
        platilloEditar.precio_sg=precio_sg
        platilloEditar.fotografia=fotografia

        platilloEditar.save()
        messages.success(request,
          'Platillo actualizado Exitosamente')
        return redirect('listado_platillo')

def listadoDetalle(request):
        detallesBdd=Detalle.objects.all()
        pedidosBdd=Pedido.objects.all()
        platillosBdd=Platillo.objects.all()
        return render(request, 'listadoDetalle.html', {'detalles':detallesBdd,'pedidos':pedidosBdd,'platillos':platillosBdd})

def guardarDetalle(request):
    id_sg_pedido=request.POST["id_sg_pedido"]
    pedidoSeleccionado=Pedido.objects.get(id_sg=id_sg_pedido)

    id_sg_platillo=request.POST["id_sg_platillo"]
    platilloSeleccionado=Platillo.objects.get(id_sg=id_sg_platillo)

    fecha_sg=request.POST["fecha_sg"]
    observaciones_sg=request.POST["observaciones_sg"]
    total_sg=request.POST["total_sg"]


    nuevoDetalle = Detalle.objects.create(
    fecha_sg = fecha_sg,
    observaciones_sg= observaciones_sg,
    total_sg= total_sg,
    pedido=pedidoSeleccionado,
    platillo=platilloSeleccionado
    )
    messages.success(request, 'El Detalle ha sido guardado exitosamente')
    return redirect('listado_detalle')

def eliminarDetalle(request,id_sg):
        detalleEliminar=Detalle.objects.get(id_sg=id_sg)
        detalleEliminar.delete()
        messages.success(request, 'El detalle ha sido eliminado exitosamente')
        return redirect('listado_detalle')

def editarDetalle(request,id_sg):
        detalleEditar=Detalle.objects.get(id_sg=id_sg)
        pedidosBdd=Pedido.objects.all()
        platillosBdd=Platillo.objects.all()
        return render(request, 'editarDetalle.html',{'detalle':detalleEditar,'pedidos':pedidosBdd,'platillos':platillosBdd})


def procesarActualizacionDetalle(request):
        id_sg=request.POST["id_sg"]
        id_sg_pedido=request.POST["id_sg_pedido"]
        pedidoSeleccionado=Pedido.objects.get(id_sg=id_sg_pedido)
        id_sg_platillo=request.POST["id_sg_platillo"]
        platilloSeleccionado=Platillo.objects.get(id_sg=id_sg_platillo)
        fecha_sg=request.POST["fecha_sg"]
        observaciones_sg=request.POST["observaciones_sg"]
        total_sg=request.POST["total_sg"]


        detalleEditar=Detalle.objects.get(id_sg=id_sg)
        detalleEditar.pedido=pedidoSeleccionado
        detalleEditar.platillo=platilloSeleccionado

        detalleEditar.save()
        messages.success(request,
          'Detalle actualizado Exitosamente')
        return redirect('listado_detalle')


def listadoIngrediente(request):
        ingredientesBdd=Ingrediente.objects.all()
        return render(request, 'listadoIngrediente.html', {'ingredientes':ingredientesBdd,})

def guardarIngrediente(request):
        nombreingrediente_sg= request.POST["nombreingrediente_sg"]
        descripcioningrediente_sg= request.POST["descripcioningrediente_sg"]
        cantidaddisponible_sg= request.POST["cantidaddisponible_sg"]
        proveedor_sg= request.POST["proveedor_sg"]


        nuevoIngrediente = Ingrediente.objects.create(
        nombreingrediente_sg=nombreingrediente_sg,
        descripcioningrediente_sg=descripcioningrediente_sg,
        cantidaddisponible_sg=cantidaddisponible_sg,
        proveedor_sg=proveedor_sg

        )
        messages.success(request, 'El ingrediente ha sido guardado exitosamente')
        return redirect('listado_ingrediente')

def eliminarIngrediente(request,id_sg):
        ingredienteEliminar=Ingrediente.objects.get(id_sg=id_sg)
        ingredienteEliminar.delete()
        messages.success(request, 'El ingrediente ha sido eliminado exitosamente')
        return redirect('listado_ingrediente')

def editarIngrediente(request,id_sg):
        ingredienteEditar=Ingrediente.objects.get(id_sg=id_sg)
        return render(request, 'editarIngrediente.html',{'ingrediente':ingredienteEditar})


def procesarActualizacionIngrediente(request):
        id_sg=request.POST["id_sg"]
        nombreingrediente_sg=request.POST["nombreingrediente_sg"]
        descripcioningrediente_sg=request.POST["descripcioningrediente_sg"]
        cantidaddisponible_sg=request.POST["cantidaddisponible_sg"]
        proveedor_sg=request.POST["proveedor_sg"]

        ingredienteEditar=Ingrediente.objects.get(id_sg=id_sg)
        ingredienteEditar.nombreingrediente_sg=nombreingrediente_sg
        ingredienteEditar.descripcioningrediente_sg=descripcioningrediente_sg
        ingredienteEditar.cantidaddisponible_sg=cantidaddisponible_sg
        ingredienteEditar.proveedor_sg=proveedor_sg


        ingredienteEditar.save()
        messages.success(request,
          'Ingrediente actualizado Exitosamente')
        return redirect('listado_ingrediente')

def listadoReceta(request):
        recetasBdd=Receta.objects.all()
        ingredientesBdd=Ingrediente.objects.all()
        platillosBdd=Platillo.objects.all()
        return render(request, 'listadoReceta.html', {'recetas':recetasBdd,'ingredientes':ingredientesBdd,'platillos':platillosBdd})

def guardarReceta(request):
        id_sg_ingrediente=request.POST["id_sg_ingrediente"]
        ingredienteSeleccionado=Ingrediente.objects.get(id_sg=id_sg_ingrediente)
        id_sg_platillo=request.POST["id_sg_platillo"]
        platilloSeleccionado=Platillo.objects.get(id_sg=id_sg_platillo)
        cantidad_sg= request.POST["cantidad_sg"]
        nombrereceta_sg= request.POST["nombrereceta_sg"]
        descripcion_sg= request.POST["descripcion_sg"]
        tiempopreparacion_sg= request.POST["tiempopreparacion_sg"]


        nuevoReceta = Receta.objects.create(
        cantidad_sg=cantidad_sg,
        ingrediente=ingredienteSeleccionado,
        platillo=platilloSeleccionado,
        nombrereceta_sg=nombrereceta_sg,
        descripcion_sg=descripcion_sg,
        tiempopreparacion_sg=tiempopreparacion_sg
        )
        messages.success(request, 'La Receta ha sido guardado exitosamente')
        return redirect('listado_receta')

def eliminarReceta(request,id_sg):
        recetaEliminar=Receta.objects.get(id_sg=id_sg)
        recetaEliminar.delete()
        messages.success(request, 'La Receta ha sido eliminado exitosamente')
        return redirect('listado_receta')

def editarReceta(request,id_sg):
        recetaEditar=Receta.objects.get(id_sg=id_sg)
        ingredientesBdd=Ingrediente.objects.all()
        platillosBdd=Platillo.objects.all()
        return render(request, 'editarReceta.html',{'receta':recetaEditar,'ingredientes':ingredientesBdd,'platillos':platillosBdd})


def procesarActualizacionReceta(request):
        id_sg=request.POST["id_sg"]
        id_sg_ingrediente=request.POST["id_sg_ingrediente"]
        ingredienteSeleccionado=Ingrediente.objects.get(id_sg=id_sg_ingrediente)
        id_sg_platillo=request.POST["id_sg_platillo"]
        platilloSeleccionado=Platillo.objects.get(id_sg=id_sg_platillo)
        cantidad_sg=request.POST["cantidad_sg"]
        nombrereceta_sg=request.POST["nombrereceta_sg"]
        descripcion_sg=request.POST["descripcion_sg"]
        tiempopreparacion_sg=request.POST["tiempopreparacion_sg"]

        recetaEditar=Receta.objects.get(id_sg=id_sg)
        recetaEditar.ingrediente=ingredienteSeleccionado
        recetaEditar.platillo=platilloSeleccionado
        recetaEditar.cantidad_sg=cantidad_sg
        recetaEditar.nombrereceta_sg=nombrereceta_sg
        recetaEditar.descripcion_sg=descripcion_sg
        recetaEditar.tiempopreparacion_sg=tiempopreparacion_sg



        recetaEditar.save()
        messages.success(request,
          'Receta actualizado Exitosamente')
        return redirect('listado_receta')
