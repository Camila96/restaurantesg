from django.contrib import admin
from .models import Provincia
from .models import *

# Register your models here.
admin.site.register(Provincia)
admin.site.register(Cliente)
admin.site.register(Pedido)
admin.site.register(Tipo)
admin.site.register(Platillo)
admin.site.register(Detalle)
admin.site.register(Ingrediente)
admin.site.register(Receta)
