from django.urls import path
from . import views

urlpatterns = [
    path('', views.listadoProvincia, name='listado_provincia'),
    path('guardarProvincia/', views.guardarProvincia, name='guardar_provincia'),
    path('eliminarProvincia/<id_sg>', views.eliminarProvincia),
    path('editarProvincia/<id_sg>', views.editarProvincia),
    path('procesarActualizacionProvincia/', views.procesarActualizacionProvincia),

    path('listado_cliente/', views.listadoCliente, name='listado_cliente'),
    path('guardarCliente/', views.guardarCliente, name='guardar_cliente'),
    path('eliminarCliente/<id_sg>', views.eliminarCliente),
    path('editarCliente/<id_sg>', views.editarCliente),
    path('procesarActualizacionCliente/', views.procesarActualizacionCliente),

    path('listado_pedido/', views.listadoPedido, name='listado_pedido'),
    path('guardarPedido/', views.guardarPedido, name='guardar_pedido'),
    path('eliminarPedido/<id_sg>', views.eliminarPedido),
    path('editarPedido/<id_sg>', views.editarPedido),
    path('procesarActualizacionPedido/', views.procesarActualizacionPedido),

    path('listado_tipo/', views.listadoTipo, name='listado_tipo'),
    path('guardarTipo/', views.guardarTipo, name='guardar_tipo'),
    path('eliminarTipo/<id_sg>', views.eliminarTipo),
    path('editarTipo/<id_sg>', views.editarTipo),
    path('procesarActualizacionTipo/', views.procesarActualizacionTipo),

    path('listado_platillo/', views.listadoPlatillo, name='listado_platillo'),
    path('guardarPlatillo/', views.guardarPlatillo, name='guardar_platillo'),
     path('eliminarPlatillo/<id_sg>' ,views.eliminarPlatillo),
    path('editarPlatillo/<id_sg>', views.editarPlatillo),
    path('procesarActualizacionPlatillo/', views.procesarActualizacionPlatillo),

    path('listado_detalle/', views.listadoDetalle, name='listado_detalle'),
    path('guardarDetalle/', views.guardarDetalle),
    path('eliminarDetalle/<id_sg>', views.eliminarDetalle),
    path('editarDetalle/<id_sg>', views.editarDetalle),
    path('procesarActualizacionDetalle/', views.procesarActualizacionDetalle),

    path('listado_ingrediente/', views.listadoIngrediente, name='listado_ingrediente'),
    path('guardarIngrediente/', views.guardarIngrediente, name='guardar_ingrediente'),
    path('eliminarIngrediente/<id_sg>', views.eliminarIngrediente),
    path('editarIngrediente/<id_sg>', views.editarIngrediente),
    path('procesarActualizacionIngrediente/', views.procesarActualizacionIngrediente),

    path('listado_receta/', views.listadoReceta, name='listado_receta'),
    path('guardarReceta/', views.guardarReceta, name='guardar_receta'),
    path('eliminarReceta/<id_sg>', views.eliminarReceta),
    path('editarReceta/<id_sg>', views.editarReceta),
    path('procesarActualizacionReceta/', views.procesarActualizacionReceta),






]
