function soloLetras(input) {
    input.value = input.value.replace(/[^a-zA-ZñÑ\s]/g, '');
}


function soloNumeros(input) {
  input.value = input.value.replace(/\D/g, '');
}

function soloTotal(input) {
  input.value = input.value.replace(/[^\d.]/g, ''); // Permite solo dígitos y un punto decimal

}
